<section class="search">
    <div class="advanced_search">
        <div class="wrapper">
            <!-- Menu Categories -->
            <div class="cats-content">
                <nav class="cats-nav">
                    <h3>Categorias</h3>
                    <ul>
                        <li><a href="shop.php">Accesorios</a></li>
                    </ul>
                    <ul>
                        <li><a href="shop.php">Asientos</a>
                        <ul>
                            <li><a href="shop.php">Butacas</a></li>
                            <li><a href="shop.php">Sillas</a></li>
                            <li><a href="shop.php">Sillones</a></li>
                            <li><a href="shop.php">Sofás</a></li>
                        </ul>
                        </li>
                    </ul>
                        <ul>
                            <li><a href="shop.php">Decoración</a><ul></ul></li>
                        </ul>
                    <ul>
                        <li><a href="shop.php">Electrodomésticos</a>
                        <ul>
                            <li><a href="shop.php">Radios</a></li>
                            <li><a href="shop.php">Televisores</a></li>
                            <li><a href="shop.php">Ventiladores</a></li>
                        </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="shop.php">Instrumentos musicales</a><ul></ul></li>
                    </ul>

                    <ul>
                        <li><a href="shop.php">Lámparas</a>
                        <ul>
                            <li><a href="shop.php">Lámaras de sobremesa</a></li>
                            <li><a href="shop.php">Lámparas de pared</a></li>
                            <li><a href="shop.php">Lámparas de techo</a></li>
                        </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="shop.php">Mesas</a>
                        <ul>
                            <li><a href="shop.php">Mesas auxiliares</a></li>
                        </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="shop.php">Relojes</a>
                        <ul>
                            <li><a href="shop.php">Relojes de pared</a></li>
                            <li><a href="shop.php">Relojes de sobremesa</a></li>
                            <li><a href="shop.php">Relojes despertadores</a></li>
                        </ul>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="shop.php">Vestuario</a>
                        <ul>
                            <li><a href="shop.php">Sombreros</a></li>
                        </ul>
                        </li>
                    </ul>

                    <h3>Estilos</h3>
                    <ul>
                        <li><a href="shop.php">40's</a></li>
                        <li><a href="shop.php">50's</a></li>
                        <li><a href="shop.php">60's</a></li>
                        <li><a href="shop.php">70's</a></li>
                        <li><a href="shop.php">Decó</a></li>
                        <li><a href="shop.php">Modernista</a></li>
                    </ul>
                </nav>
            </div>
            <!-- /Menu Categories -->
        </div>
    </div><!--  end advanced search section  -->
</section><!--  end search section  -->