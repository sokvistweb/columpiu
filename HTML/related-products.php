<section class="wrapper margin-top featured" id="">
    <h2 class="row-title"><a href="#">Productos relacionados</a></h2>
    <div class="row">
        <div class="column">
            <div class="owl-carousel owl-theme owl-skv-theme" id="owl-carousel">
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="#"><img src="assets/images/featured-1.jpg" alt="Featured product" width="400" height="400"></a>
                    </div>
                    <a href="single-product.php" class="buy-btn">Comprar</a>
                    <h3 class="product-name"><a href="single-product.php">Juego de te</a></h3>
                </div>
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="single-product.php"><img src="assets/images/featured-2.jpg" alt="Featured product" width="400" height="400"></a>
                    </div>
                    <a href="single-product.php" class="buy-btn">Comprar</a>
                    <h3 class="product-name"><a href="single-product.php">Reloj despertador</a></h3>
                </div>
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="single-product.php"><img src="assets/images/featured-3.jpg" alt="Featured product" width="400" height="400"></a>
                    </div>
                    <a href="#" class="buy-btn">Comprar</a>
                    <h3 class="product-name"><a href="single-product.php">Butaca sillón</a></h3>
                </div>
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="single-product.php"><img src="assets/images/featured-4.jpg" alt="Featured product" width="400" height="400"></a>
                    </div>
                    <a href="#" class="buy-btn">Comprar</a>
                    <h3 class="product-name"><a href="single-product.php">Juego de te</a></h3>
                </div>
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="single-product.php"><img src="assets/images/featured-5.jpg" alt="Featured product" width="400" height="400"></a>
                    </div>
                    <a href="#" class="buy-btn">Comprar</a>
                    <h3 class="product-name"><a href="single-product.php">Juego de te</a></h3>
                </div>
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="single-product.php"><img src="assets/images/featured-6.jpg" alt="Featured product" width="400" height="400"></a>
                    </div>
                    <a href="#" class="buy-btn">Comprar</a>
                    <h3 class="product-name"><a href="single-product.php">Juego de te</a></h3>
                </div>
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="single-product.php"><img src="assets/images/featured-7.jpg" alt="Featured product" width="400" height="400"></a>
                    </div>
                    <a href="#" class="buy-btn">Comprar</a>
                    <h3 class="product-name"><a href="single-product.php">Juego de te</a></h3>
                </div>
            </div>
        </div>
    </div>
</section>