<section class="wrapper margin-top" id="blog-section">
    <hr>
    <div class="row row-center">
       <div class="column column-20">
            <h2 class="row-title"><a href="blog.php">Universo Columpiu</a></h2>
        </div>

        <div class="column">
            <div class="grid-container">
                <div class="grid-box a">
                    <a href="single-post.php">
                        <h3>Escaparates de Columpiu</h3>
                        <img src="assets/images/blog-featured-1.jpg" alt="" width="600" height="600">
                    </a>
                </div>
                <div class="grid-box b">
                    <a href="single-post.php">
                        <h3>Columpiu in the world</h3>
                        <img src="assets/images/blog-featured-5.jpg" alt="" width="600" height="600">
                    </a>
                </div>
                <div class="grid-box c">
                    <a href="single-post.php">
                        <h3>Historia de Columpiu</h3>
                        <img src="assets/images/blog-featured-2.jpg" alt="" width="600" height="600">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>