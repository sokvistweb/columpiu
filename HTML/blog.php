<?php include("header.php"); ?>
   

<section class="wrapper margin-top-20 page">
    <div class="row">
        <div class="column">
            <h1>Universo Columpiu</h1>
            <hr />
        </div>
    </div>
    
    <div class="row">
        <div class="column">
            <ul class="blog-grid posts">
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-post.php"><img src="assets/images/blog-featured-1.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="post-title"><a href="single-post.php">Australia releases rare marsupial bilby into the wild in NSW</a></h2>
                    <p>A rare marsupial that once ran wild in Australia's New South Wales has been reintroduced into the state for the first time in more than a century.</p>
                    <div>
                        <a href="single-post.php" class="read">Leer</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-post.php"><img src="assets/images/blog-featured-2.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="post-title"><a href="single-post.php">Culture and multilingualism meet in Leeuwarden</a></h2>
                    <p>“Achievements, benefits, and future perspectives in the various fields and domains: Creative Culture & Circular Economy; Language Use and Linguistic Diversity”</p>
                    <div>
                        <a href="single-post.php" class="read">Leer</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-post.php"><img src="assets/images/blog-featured-3.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="post-title"><a href="single-post.php">The first Campus on language planning takes place in Udine</a></h2>
                    <p>Forty language policy makers from regions all over Europe attended the first edition of our NPLD-Coppieters Campus on Planning and…</p>
                    <div>
                        <a href="single-post.php" class="read">Leer</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-post.php"><img src="assets/images/blog-featured-4.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="post-title"><a href="single-post.php">“New technologies should be used to implement linguistic rights”</a></h2>
                    <p>20th anniversary of the Framework Convention for the Protection of National Minorities and the European Charter for Regional or Minority languages (ECRML)</p>
                    <div>
                        <a href="single-post.php" class="read">Leer</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-post.php"><img src="assets/images/blog-featured-5.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="post-title"><a href="single-post.php">The opening ceremony of Pompeu Fabra Year vindicates the Catalan as a language for everyone</a></h2>
                    <p>Catalonia commemorates in 2018 the figure and the work of the Catalan linguist Pompeu Fabra on occasion of the 150 anniversary of his birth.</p>
                    <div>
                        <a href="single-post.php" class="read">Leer</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-post.php"><img src="assets/images/blog-featured-6.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="post-title"><a href="single-post.php">“What is the role of CRSS languages in boosting growth and jobs in Europe?”</a></h2>
                    <p>Conclusions of the EU report “EU Strategy on Multilingualism. Costs and Benefits” were presented during the Conference.</p>
                    <div>
                        <a href="single-post.php" class="read">Leer</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="row margin-top-50">
        <div class="column">
            <ul class="pagination">
                <li><span class="disabled">Ant.</span></li>
                <li><a href="#" class="active">1</a></li>
                <li><a href="#" class="">2</a></li>
                <li><a href="#" class="">3</a></li>
                <li><span>&hellip;</span></li>
                <li><a href="#" class="">8</a></li>
                <li><a href="#" class="">9</a></li>
                <li><a href="#" class="">10</a></li>
                <li><a href="#" class="">Sig.</a></li>
            </ul>
        </div>
    </div>
    
    <hr class="alt" />
    
</section>

<?php include("featured-products.php"); ?>

<?php include("featured-blog.php"); ?>

<?php include("footer.php"); ?>