<?php include("header.php"); ?>
   

<section class="wrapper margin-top-40" id="arrivals">
    <div class="row">
        <div class="column">
            <h2 class="row-title"><a href="shop.php">Novedades</a></h2>
             <!-- Slider -->
            <ul class="rslides" id="slider-main">
                <li><div class="scale-effect"><a href="shop.php"><img src="assets/images/slider-1.jpg" alt="Columpiu" width="680" height="680"></a></div></li>
                <li><div class="scale-effect"><a href="shop.php"><img src="assets/images/slider-2.jpg" alt="Columpiu" width="680" height="680"></a></div></li>
                <li><div class="scale-effect"><a href="shop.php"><img src="assets/images/slider-3.jpg" alt="Columpiu" width="680" height="680"></a></div></li>
            </ul>
        </div>

        <div class="column column-3"></div>

        <div class="column">
            <h1 class="row-title"><a href="#">Tienda de muebles</a></h1>
            <div class="our-story-image scale-effect">
                <a href="shop.php"><img src="assets/images/services.jpg" alt="" width="680" height="680"></a>
            </div>
        </div>
    </div>
</section>


<section class="wrapper margin-top" id="main-ctas">
    <div class="row">
        <div class="column w-tagline">
            <h2 class="row-title"><a href="page.php">Vaciados</a></h2>
            <p>Valoramos y vaciamos tu piso</p>
            <div class="scale-effect">
                <a href="page.php"><img src="assets/images/img_2.jpg" alt="" width="600" height="600"></a>
            </div>
        </div>

        <div class="column w-tagline">
            <h2 class="row-title"><a href="page-image.php">Atrezzo</a></h2>
            <p>Alguiler de artículos cotidianos</p>
            <div class="scale-effect">
                <a href="page-image.php"><img src="assets/images/img_3.jpg" alt="" width="600" height="600"></a>
            </div>
        </div>

        <div class="column w-tagline">
            <h2 class="row-title"><a href="shop.php">Ofertas</a></h2>
            <div class="scale-effect">
                <a href="shop.php"><img src="assets/images/img_1.jpg" alt="" width="600" height="600"></a>
            </div>
        </div>
    </div>
</section>


<section class="wrapper margin-top" id="sub-headers">
    <div class="row row-center">
        <div class="column">
            <div>
                <h3 class="row-title"><a href="page.php" class="a-dotted" title="Servicio de transporte">Tenemos servicio de transporte</a></h3>
            </div>
        </div>
        
        <div class="column">
            <div>
                <h3 class="row-title">Todos los artículos son originales de época</h3>
            </div>
        </div>
    </div>
</section>


<?php include("featured-products.php"); ?>


<?php include("featured-blog.php"); ?>


<?php include("footer.php"); ?>