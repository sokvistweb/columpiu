<?php include("header.php"); ?>
  
  
<section class="wrapper">
    <ul id="breadcrumbs" class="breadcrumbs">
        <li class="item-home"><a class="bread-link bread-home" href="index.php" title="Homepage">Inicio</a></li>
        <li class="separator separator-36"> &gt; </li>
        <li class="item-current item-1222"><span title="Mesas">Mesas</span></li>
    </ul>
</section>
   

<section class="wrapper margin-top-20 page">
    <div class="row">
        <div class="column">
            <h1>Tienda</h1>
            <hr />
        </div>
    </div>
    
    <div class="row">
        <div class="column">
            <ul class="shop-grid products">
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php"><img src="assets/images/slider-1.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Juego de te</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">90,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart special" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/slider-2.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Reloj despertador</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">190,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/slider-3.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Calendario de sobremesa</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">200,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-1.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Juego de te</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">390,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-2.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Despertador</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">90,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-3.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Sillón</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">859,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-4.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Juego de te</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">9,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-5.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Juego de te</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">9,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product-2.php">
                            <img src="assets/images/featured-6.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product-2.php">Juego de te</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">9,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product-2.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-3.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Sillón</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">859,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-4.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Juego de te</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">9,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
                <li class="product-card">
                    <div class="scale-effect">
                        <a href="single-product.php">
                            <img src="assets/images/featured-2.jpg" alt="Columpiu" width="680" height="680">
                        </a>
                    </div>
                    <h2 class="product-name woocommerce-loop-product__title"><a href="single-product.php">Despertador</a></h2>
                    <span class="price">
                        <span class="woocommerce-Price-amount amount">90,00<span class="woocommerce-Price-currencySymbol">€</span></span>
                    </span>
                    <div>
                        <a href="single-product.php" data-quantity="1" class="buy-btn product_type_simple add_to_cart_button ajax_add_to_cart" rel="nofollow">Añadir al carrito</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="row margin-top-50">
        <div class="column">
            <ul class="pagination">
                <li><span class="disabled">Ant.</span></li>
                <li><a href="#" class="active">1</a></li>
                <li><a href="#" class="">2</a></li>
                <li><a href="#" class="">3</a></li>
                <li><span>&hellip;</span></li>
                <li><a href="#" class="">8</a></li>
                <li><a href="#" class="">9</a></li>
                <li><a href="#" class="">10</a></li>
                <li><a href="#" class="">Sig.</a></li>
            </ul>
        </div>
    </div>
    
</section>

<?php include("featured-ofertas.php"); ?>

<?php include("featured-novedades.php"); ?>

<?php include("footer.php"); ?>