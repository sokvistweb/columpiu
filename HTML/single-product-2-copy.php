<?php include("header.php"); ?>
   

<section class="wrapper margin-top-50 page">
    
    <div class="row">
        <div class="column">
            <div class="owl-carousel owl-theme owl-skv-theme" id="owl-carousel-product">
                <div><img src="assets/images/product-1.jpg" alt="Columpiu" width="585" height="585"></div>
                <div><img src="assets/images/product-1.jpg" alt="Columpiu" width="585" height="585"></div>
                <div><img src="assets/images/product-1.jpg" alt="Columpiu" width="585" height="585"></div>
                <div><img src="assets/images/product-1.jpg" alt="Columpiu" width="585" height="585"></div>
            </div>
        </div>
        
        <div class="column">

            <div class="summary entry-summary">
                <h1 class="product_title entry-title">Juego de te</h1>

                <p class="price"><span class="woocommerce-Price-amount amount">290,00<span class="woocommerce-Price-currencySymbol">€</span></span></p>

                <div class="woocommerce-product-details__short-description">
                    <p>Sed vulputate odio ut enim blandit volutpat maecenas. Malesuada proin libero nunc consequat interdum varius.</p>
                </div>

                <form class="cart" action="http://columpiu/producto/juego-de-te/" method="post" enctype="multipart/form-data">

                    <div class="quantity">
                        <label class="screen-reader-text" for="quantity_5c06997107204">Cantidad</label>
                        <input type="number" id="quantity_5c06997107204" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Cantidad" size="4" pattern="[0-9]*" inputmode="numeric" aria-labelledby="">
                    </div>

                    <button type="submit" name="add-to-cart" value="24" class="single_add_to_cart_button button alt">Añadir al carrito</button>

                </form>

                <div class="product_meta">
                    <span class="posted_in">Categorías: <a href="#" rel="tag">Accesorios</a>, <a href="#" rel="tag">Lámparas</a>, <a href="#" rel="tag">Lámparas de pared</a></span>
                </div>

            </div><!-- /.summary -->

            <div class="details wc-product-details">
                <div class="tabs-container">
                    <div class="tabs">
                        <ul class="tab-links">
                            <li class="active"><a href="#tab1">Descripción</a></li>
                            <li><a href="#tab2">Información adicional</a></li>
                            <li><a href="#tab3">Comparte</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="panel entry-content tab active" id="tab1">
                                <p>Semper feugiat nibh sed pulvinar proin. Proin nibh nisl condimentum id venenatis a. Vulputate odio ut enim blandit volutpat. Eu scelerisque felis imperdiet proin. Egestas integer eget aliquet nibh praesent. Imperdiet dui accumsan sit amet nulla facilisi. Blandit cursus risus at ultrices mi tempus.</p>
                            </div>

                            <div class="panel entry-content tab" id="tab2">
                                <span>Dimensiones: 40 x 30 x 60 cm</span>
                            </div>
                            
                            <div class="share-icons tab" id="tab3">
                                <ul class="icons">
                                    <li>
                                        <a href="#" title="Comparte en Twitter" class="twitter" target="_blank">
                                            <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-twitter"></use></svg>
                                            <span class="label">Twitter</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="facebook" target="_blank">
                                            <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-facebook"></use></svg>
                                            <span class="label">Facebook</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Comparte en Google+" class="googleplus" target="_blank">
                                            <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-instagram"></use></svg>
                                            <span class="label">Google+</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" data-action="share/whatsapp/share" title="Comparte en WhatsApp" class="whatsapp" target="_blank">
                                            <svg class="icon"><use xlink:href="assets/images/symbol-defs.svg#icon-social-whatsapp"></use></svg>
                                            <span class="label">WhatsApp</span>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /.share-icons -->
                        </div>
                    </div>
                </div>
            </div><!-- /.wc-product-details -->
            
        </div>
    </div>
    
    <div class="row">
        <div class="column">
            
        </div>
    </div>
    
    <hr class="alt" />
</section>

<?php include("related-products.php"); ?>

<?php include("footer.php"); ?>