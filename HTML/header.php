<!DOCTYPE html>
<html lang="es">
<head>
	<title>Columpiu</title>
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="description" content=""/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	
	<link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" /><!-- 57×57px -->
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png"><!-- 180×180px -->
	
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!--<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,700" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Cabin+Condensed:400,700|Fira+Sans+Condensed:700|Fira+Sans:400,700" rel="stylesheet">
    
    <!-- Open Graph -->
    <meta property="og:locale" content="es-ES">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Columpiu - Els mobles de la iaia">
    <meta property="og:description" content="">
    <meta property="og:image" content="assets/images/og-image.jpg">
    <meta property="og:url" content="https://www.columpiu.com/">
    <meta property="og:site_name" content="Columpiu">
    
    
</head>
<body>

	<header>
		<div class="wrapper">
            <a href="index.php" class="a-logo">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 160"><g><path d="M111 34.4c-19.8 0-32.8 13-32.8 31.3s13 31.5 32.8 31.5c19.8 0 32.8-13.2 32.8-31.5S130.9 34.4 111 34.4zm0 43.8c-6 0-9.2-5.1-9.2-12.6s3.2-12.3 9.2-12.3c6 0 9.2 4.8 9.2 12.3 0 7.6-3.2 12.6-9.2 12.6zm50.5 18.2h23.3V4.8h-23.3v91.6zm81.8-29.8c0 5.6-2.5 10.2-7.7 10.2-5.3 0-7.8-3.9-7.8-9.9V35.2h-23.3v37.7c0 13.2 6.5 24.3 22.1 24.3 10 0 15.2-4.8 17.9-11.7h.3c0 2.5 1.2 8.2 2.2 11.5l19.7-.6V35.2h-23.3v31.4zM39.4 78.4c-8.3 0-11.8-5.4-11.8-13.4 0-6.9 3.2-11.7 8.3-11.7 4.5 0 6.1 3.6 6.1 6.9 0 1.2-.3 2.2-.3 2.2l20.1-1s.5-1.9.5-4.8c0-12.3-9.2-22.2-25.8-22.2C17 34.4 4 47.1 4 65.9c0 18 11.8 31.3 33.6 31.3 16.1 0 24.8-7.3 27-10.5L54.2 72.2c-1.3 1.5-7 6.2-14.8 6.2zm326.4-44c-10.9 0-16.2 5.6-18.7 13.4-2.9-7.9-9.2-13.4-20-13.4-10 0-15.2 4.9-17.9 11.7h-.4c0-2.5-1.3-8.7-2.7-11.5l-19.2.6v61.2h23.3V65c0-5.3 1.6-10.2 7.8-10.2 5.3 0 7.5 3.8 7.5 9.9v31.8h23.4V65c0-5.3 1.6-10.2 7.8-10.2 5.3 0 7.5 3.8 7.5 9.9v31.8h23.3V58.7c.1-13.4-6.6-24.3-21.7-24.3zm81.8 0c-9.2 0-14.8 4.2-17.9 10.4h-.5c0-1.2-1-6.9-2.6-10.2l-19.3.6v87.7h23.3V90.7c3 3.8 7.9 6.5 15.8 6.5 14.3 0 26.5-10.5 26.5-31.4 0-17.9-9.3-31.4-25.3-31.4zm-7.4 43.2c-5.3 0-9.6-4.4-9.6-9.1v-5.6c0-5.5 4.8-9 9.6-9 5.4 0 9.1 4.4 9.1 11.8s-3.6 11.9-9.1 11.9zm62-75.8c-8.2 0-14.5 5.8-14.5 14s6.4 13.9 14.5 13.9c8.2 0 14.7-5.7 14.7-13.9 0-8.1-6.5-14-14.7-14zm-11.4 94.6h23.3V34.5l-23.3.6v61.3zm81.9-61.2v31.4c0 5.6-2.5 10.2-7.7 10.2-5.3 0-7.8-3.9-7.8-9.9V35.2h-23.3v37.7c0 13.2 6.5 24.3 22.1 24.3 10 0 15.2-4.8 17.9-11.7h.3c0 2.5 1.2 8.2 2.2 11.5l19.7-.6V35.2h-23.4zM458.2 152.7l1.4-10.9h-5.2c-.5.9-1 2.3-1 2.6h-.1c-.6-1.7-1.9-2.8-4.3-2.8-4.6 0-7.6 4.3-7.6 9.7 0 4.7 2.6 7.1 5.9 7.1 2.6 0 3.9-1.2 4.8-2.8h.1c.2 1.8 1.4 2.8 4.2 2.8 1.7 0 2.9-.3 2.9-.3l.4-4.5c-.2.1-.4.1-.6.1-.7-.1-1-.3-.9-1zm-6-1.9c-.2 1.2-1.4 2.4-2.6 2.4-1.1 0-1.8-1-1.8-2.6 0-2.3 1.1-3.7 2.6-3.7 1.1 0 2.1.9 2.1 2.2l-.3 1.7zm-188.6 7.4h6.2l3-24.5h-6.2l-3 24.5zm-29.5-16.6c-5.7 0-9.6 4-9.6 9.4 0 4.3 2.8 7.4 7.6 7.4 5.7 0 9.6-4 9.6-9.4-.1-4.4-2.9-7.4-7.6-7.4zm-1.5 11.7c-1.2 0-1.8-1.1-1.8-2.7 0-2.3.9-4 2.7-4 1.2 0 1.9 1 1.9 2.6-.1 2.4-1 4.1-2.8 4.1zm23.5-11.7c-2.3 0-3.7 1.1-4.6 2.3l1.3-10.2h-6.2l-3 24.5h5c.4-.9.7-1.9.7-2.6h.1c.7 1.7 2.2 2.8 4.4 2.8 4.2 0 8-3.7 8-9.8-.1-4.6-2.6-7-5.7-7zm-3.4 11.5c-1.1 0-2.2-.9-2.1-2.3l.2-1.7c.2-1.2 1.3-2.3 2.6-2.3 1.2 0 1.9.9 1.9 2.5 0 2.4-1.1 3.8-2.6 3.8zm-90.1-19.4l-3 24.5h6.2l3-24.5h-6.2zm-22.1 24.5h15.7l.7-5.3h-9.2l.5-3.7h7.7l.6-4.9h-7.7l.5-3.7h9.2l.7-5.3h-15.7l-3 22.9zm38.2-16.6c-4.9 0-8.1 2.1-8.1 6.1 0 5.2 7.4 4.3 7.4 6.1 0 .5-.5.7-1.4.7-2.3 0-4.8-2-4.8-2l-3 3.6c-.1 0 1.9 2.4 7.7 2.4 5.2 0 8-2.5 8-5.7 0-5.5-8.2-3.5-8.2-6 0-.7.6-1.3 1.7-1.3 1 0 1.4.6 1.4 1.4 0 .4-.1.7-.1.7l5.4-.3s.3-.6.3-1.4c0-2.6-2.5-4.3-6.3-4.3zm41.8 11.2l.6-4.7c.4-3.6-.9-6.5-4.8-6.5-2.8 0-4.4 1.5-5.2 3.5-.5-2.1-2-3.5-4.7-3.5-2.6 0-4.1 1.3-5 3.1h-.1c.1-.7-.1-2.3-.3-3.1l-5.2.2-2 16.4h6.2l1-8.6c.2-1.4.7-2.6 2.2-2.6 1.3 0 1.7 1 1.5 2.6l-1 8.5h6.2l1.1-8.7c.2-1.4.8-2.4 2.1-2.4s1.7 1 1.5 2.6l-.6 4.6c-.3 2.5.7 4.1 4 4.1 2.1 0 3.4-.3 3.4-.3l.4-4.6c-.2 0-.3.1-.6.1-.5.1-.8-.2-.7-.7zm185-19.9c-2.4 0-4.4 1.8-4.4 4.2 0 1.9 1.5 3.3 3.5 3.3 2.3 0 4.4-1.7 4.4-4.1 0-2-1.5-3.4-3.5-3.4zm17.6 8.9c-.5.9-1 2.3-1 2.6h-.1c-.6-1.7-1.9-2.8-4.3-2.8-4.6 0-7.6 4.3-7.6 9.7 0 4.7 2.6 7.1 5.9 7.1 2.6 0 3.9-1.2 4.8-2.8h.1c.2 1.8 1.4 2.8 4.2 2.8 1.7 0 2.9-.3 2.9-.3l.4-4.5c-.2.1-.4.1-.6.1-.5 0-.8-.2-.7-.9l1.4-10.9h-5.4zm-2.2 9c-.2 1.2-1.4 2.4-2.6 2.4-1.1 0-1.8-1-1.8-2.6 0-2.3 1.1-3.7 2.6-3.7 1.1 0 2.1.9 2.1 2.2l-.3 1.7zm-59 7.4h6.2l3-24.5h-6.2l-3 24.5zm23.1-16.4c-.5.9-1 2.3-1 2.6h-.1c-.6-1.7-1.9-2.8-4.3-2.8-4.6 0-7.6 4.3-7.6 9.7 0 4.7 2.6 7.1 5.9 7.1 2.6 0 3.9-1.2 4.8-2.8h.1c.2 1.8 1.4 2.8 4.2 2.8 1.7 0 2.9-.3 2.9-.3l.4-4.5c-.2.1-.4.1-.6.1-.5 0-.8-.2-.7-.9l1.4-10.9H385zm-2.1 9c-.2 1.2-1.4 2.4-2.6 2.4-1.1 0-1.8-1-1.8-2.6 0-2.3 1.1-3.7 2.6-3.7 1.1 0 2.1.9 2.1 2.2l-.3 1.7zm-99.8-9.2c-5.5 0-9.4 4-9.4 9.4 0 4.3 2.7 7.4 7.9 7.4 4.1 0 6.7-1.8 7.4-2.7l-2.1-3.4c-.7.6-1.8 1.6-3.9 1.6-2 0-2.9-1.1-3.1-2.6h9.4s.5-1.2.5-2.9c-.1-4.1-2.5-6.8-6.7-6.8zm1.2 7h-4.4c.4-1.8 1.5-2.4 2.7-2.4 1.2 0 1.7 1 1.7 2.2v.2zm117.1-6.8l-1.5 12.5c-.3 2.5.7 4.1 4 4.1 2.1 0 3.2-.3 3.2-.3l.4-4.5c-.1 0-.3.1-.6.1s-.6-.2-.6-.6l1.4-11.4-6.3.1zm-54.9-.2c-5.5 0-9.4 4-9.4 9.4 0 4.3 2.7 7.4 7.9 7.4 4.1 0 6.7-1.8 7.4-2.7l-2.1-3.4c-.7.6-1.8 1.6-3.9 1.6-2 0-2.9-1.1-3.1-2.6h9.4s.5-1.2.5-2.9c-.2-4.1-2.5-6.8-6.7-6.8zm1.1 7h-4.4c.4-1.8 1.5-2.4 2.7-2.4 1.2 0 1.7 1 1.7 2.2v.2zm85-6.8l-1.5 12.5c-.3 2.5.7 4.1 4 4.1 2.1 0 3.2-.3 3.2-.3l.4-4.5c-.1 0-.3.1-.6.1s-.6-.2-.6-.6l1.4-11.4-6.3.1zm4.2-8.9c-2.4 0-4.4 1.8-4.4 4.2 0 1.9 1.5 3.3 3.5 3.3 2.3 0 4.4-1.7 4.4-4.1-.1-2-1.5-3.4-3.5-3.4zM301 141.6c-4.9 0-8.1 2.1-8.1 6.1 0 5.2 7.4 4.3 7.4 6.1 0 .5-.5.7-1.4.7-2.3 0-4.8-2-4.8-2l-3 3.6c-.1 0 1.9 2.4 7.7 2.4 5.2 0 8-2.5 8-5.7 0-5.5-8.2-3.5-8.2-6 0-.7.6-1.3 1.7-1.3 1 0 1.4.6 1.4 1.4 0 .4-.1.7-.1.7l5.4-.3s.3-.6.3-1.4c-.1-2.6-2.6-4.3-6.3-4.3zm28.1-7.9l-1.2 9.6c-.6-1-1.8-1.7-3.8-1.7-3.8 0-7.9 3.3-7.9 9.8 0 4 1.9 7 5.6 7 2.5 0 4.1-1.2 5.1-2.9h.1c.2 1.9 1.3 2.9 4.2 2.9 1.7 0 2.9-.3 2.9-.3l.4-4.5c-.2.1-.4.1-.6.1-.5 0-.8-.2-.7-.9l2.3-19h-6.4zm-2.1 17.1c-.2 1.4-1.5 2.3-2.6 2.3-1.2 0-1.9-1-1.9-2.5 0-2.3 1.1-3.9 2.6-3.9 1.2 0 2.1 1.1 2.1 2.2l-.2 1.9z"/></g></svg>
            </a>
			<a href="#" class="menu_icon" id="menu_icon"></a>
			<nav>
                <ul class="main-menu">
                    <li><a href="shop.php">Novedades</a></li>
                    <li><a href="shop.php">Tienda</a></li>
                    <li><a href="page.php">Vaciados</a></li>
                    <li><a href="page-image.php">Atrezzo</a></li>
                    <li><a href="shop.php">Ofertas</a></li>
                </ul>
                <div class="shop-menu">
                    <span class="search-prods"><a href="#" class="advanced_search_icon" id="advanced_search_btn">Productos</a></span>
                    <ul class="shop-acces">
                        <li><a href="#" class="login_btn">Login</a></li>
                        <li><a href="/cesta/" class="bag" title="Tu bolsa de la compra"><svg class="svg-icon"><use xlink:href="assets/images/symbol-defs.svg#icon-ios-cart"></use></svg><span>Bolsa de la compra</span></a></li>
                    </ul>
                    <div class="woo-items-count">
                        <a class="cart-contents" href="#" title="Tu bolsa de la compra">
                            <span class="cart-contents-count">2</span>
                        </a>
                    </div>
                    
                    <!--
                    <div class="search-box">
                        <input type="text" placeholder="Buscar" class="m-input">
                        <button type="submit" value="Buscar" class="search-box-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg>
                        </button>
                    </div> /.search -->
                    
                    <div class="mobile-search">
                        <form class="form-search" role="search" method="get" action="http://columpiu/">
                           <label class="screen-reader-text" for="woocommerce-product-search-field-0">Buscar por:</label>
                            <input class="form-control form-text" maxlength="128" placeholder="Buscar" size="15" type="text" />
                            <button type="submit" value="Buscar" class="search-box-btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg>
                            </button>
                            <input type="hidden" name="post_type" value="product">
                        </form>
                    </div>
                </div>
            </nav>
            
            <div class="widget woocommerce widget_shopping_cart">
                <!--<div class="widget_shopping_cart_content">
                    <p class="woocommerce-mini-cart__empty-message">No hay productos en el carrito.</p>
                </div>-->
                
                <!--<div class="cart-dropdown">
                    <div class="cart-dropdown-inner">
                        <div class="dropdown-cart-wrap isempty">
                            <p>Tu bolsa de la compra está vacía.</p>
                        </div>
                    </div>
                </div>-->
                
                <div class="cart-dropdown">
                    <div class="cart-dropdown-inner">

                        <div class="dropdown-cart-wrap">
                            <div class="dropdown-cart-left">
                                <img src="assets/images/slider-3.jpg" alt="Columpiu">
                            </div>

                            <div class="dropdown-cart-right">
                                <h3>Calendario de sobremesa</h3>
                                <p>90€</p>
                            </div>
                        </div>

                        <div class="dropdown-cart-wrap">
                            <div class="dropdown-cart-left">
                                <img src="assets/images/slider-1.jpg" alt="Columpiu">
                            </div>

                            <div class="dropdown-cart-right">
                                <h3>Juego de te</h3>
                                <p>290€</p>
                            </div>
                        </div>

                        <div class="dropdown-cart-wrap wc-subtotal">
                            <p>Subtotal: <span><span class="woocommerce-Price-amount amount">255,00<span class="woocommerce-Price-currencySymbol">€</span></span></span></p>
                        </div>

                        <div class="dropdown-cart-wrap dropdown-cart-last">
                            <a href="#" class="add_to_cart_button">Ver bolsa</a>
                            <a href="#" class="add_to_cart_button">Confirmar</a>
                        </div>
                    </div>
                </div><!-- /cart-dropdown -->
                
            </div>
            
		</div>
		
	</header><!--  End Header  -->
	
	
	<?php include("menu-categories.php"); ?>
	
	
	<section class="site-search">
        <div class="wrapper">
            <div class="widget woocommerce widget_product_search">
            <form role="search" method="get" class="woocommerce-product-search" action="http://columpiu/">
                <label class="screen-reader-text" for="woocommerce-product-search-field-0">Buscar por:</label>
                <input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Buscar productos…" value="" name="s">
                <button type="submit" value="Buscar">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path></svg>
                </button>
                <input type="hidden" name="post_type" value="product">
            </form>
            </div>
        </div>
    </section>