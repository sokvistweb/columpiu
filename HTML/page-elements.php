<?php include("header.php"); ?>
   

<section class="wrapper margin-top-40 page">
    <div class="row">
        <div class="column">
        
            <h1>Elementos de una página</h1>
            <hr />
            <h2>Contenido de prueba</h2>
            
            <p>Praesent ac adipiscing ullamcorper semper ut amet ac risus. Lorem sapien ut odio odio nunc. Ac adipiscing nibh porttitor erat risus justo adipiscing adipiscing amet placerat accumsan. Vis. Faucibus odio magna tempus adipiscing a non. In mi primis arcu ut non accumsan vivamus ac blandit adipiscing adipiscing arcu metus praesent turpis eu ac lacinia nunc ac commodo gravida adipiscing eget accumsan.</p>
            <p>Faucibus odio magna tempus adipiscing a non. In mi primis arcu ut non accumsan vivamus ac blandit adipiscing adipiscing ac nunc adipiscing adipiscing lorem ipsum dolor sit amet nullam veroeros adipiscing.</p>
            
            <!-- Elements -->
            <h2>Texto</h2>
            <p>Este texto es <b>bold</b> y este es <strong>strong</strong>. Este es <i>italic</i> y este es <em>emphasized</em>. Este es <u>subrayado</u> y este es un <a class="a-reverse" href="#">link inverso</a>.</p>
            <hr />
            <h2>Encabezado nivel 2</h2>
            <h3>Encabezado nivel 3</h3>
            <h4>Encabezado nivel 4</h4>
            <hr />
            <p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
        </div>
    </div>
    
    <!-- Lists -->
    <h2>Listas</h2>
    <div class="row">
       
        <div class="column">
            
            <h3>No ordenada</h3>
            <ul>
                <li>Dolor etiam magna etiam.</li>
                <li>Sagittis lorem eleifend.</li>
                <li>Felis dolore viverra.</li>
                <li>Sagittis lorem eleifend.</li>
                <li>Felis feugiat viverra.</li>
                <li>Etiam vel lorem sed viverra.</li>
            </ul>
        </div>
        <div class="column">
            
            <h3>Ordenada</h3>
            <ol>
                <li>Dolor etiam magna etiam.</li>
                <li>Etiam vel lorem sed viverra.</li>
                <li>Felis dolore viverra.</li>
                <li>Dolor etiam magna etiam.</li>
                <li>Etiam vel lorem sed viverra.</li>
                <li>Felis dolore viverra.</li>
            </ol>
        </div>
    </div>
    <div class="row margin-top-40">
        <div class="column">
            
            <h3>Lista de definición</h3>
            <dl>
                <dt>Item1</dt>
                <dd>
                    <p>Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Lorem ipsum dolor.</p>
                </dd>
                <dt>Item2</dt>
                <dd>
                    <p>Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Lorem ipsum dolor.</p>
                </dd>
                <dt>Item3</dt>
                <dd>
                    <p>Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Lorem ipsum dolor.</p>
                </dd>
            </dl>
            
        </div>
    </div>
    <div class="row margin-top-40">
        <div class="column">
            
            <h2>Paginación</h2>
            <ul class="pagination">
                <li><span class="disabled">Ant.</span></li>
                <li><a href="#" class="active">1</a></li>
                <li><a href="#" class="">2</a></li>
                <li><a href="#" class="">3</a></li>
                <li><span>&hellip;</span></li>
                <li><a href="#" class="">8</a></li>
                <li><a href="#" class="">9</a></li>
                <li><a href="#" class="">10</a></li>
                <li><a href="#" class="">Sig.</a></li>
            </ul>
            
        </div>
    </div>
    <div class="row margin-top-40">
        <div class="column">

        <!-- Blockquote -->
            <h2>Blockquote (cita)</h2>
            <blockquote>Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Lorem ipsum dolor. Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus.</blockquote>
            
        </div>
    </div>
    <div class="row margin-top-40">
        <div class="column">

        <!-- Table -->
            <h2>Tabla</h2>

            <div class="table-wrapper">
                <table>
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Item1</td>
                            <td>Ante turpis integer aliquet porttitor.</td>
                            <td>29.99</td>
                        </tr>
                        <tr>
                            <td>Item2</td>
                            <td>Vis ac commodo adipiscing arcu aliquet.</td>
                            <td>19.99</td>
                        </tr>
                        <tr>
                            <td>Item3</td>
                            <td> Morbi faucibus arcu accumsan lorem.</td>
                            <td>329.00</td>
                        </tr>
                        <tr>
                            <td>Item4</td>
                            <td>Vitae integer tempus condimentum.</td>
                            <td>9.99</td>
                        </tr>
                        <tr>
                            <td>Item5</td>
                            <td>Ante turpis integer aliquet porttitor.</td>
                            <td>229.99</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td>100.00</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="row margin-top-40">
        <div class="column">
            
            <!-- Form -->
            <h2>Formulario</h2>

            <form method="post" action="#">
                <div class="row">
                    <div class="column column-40">
                        <input type="text" name="demo-name" id="demo-name" value="" placeholder="Name" />

                        <input type="email" name="demo-email" id="demo-email" value="" placeholder="Email" />

                        <!-- Break -->
                        <div class="select-wrapper">
                            <select name="demo-category" id="demo-category">
                                <option value="">- Categoria -</option>
                                <option value="1">Sillas</option>
                                <option value="1">Mesas</option>
                                <option value="1">Camas</option>
                                <option value="1">Cocina</option>
                            </select>
                        </div>
                        
                        <input type="submit" value="Enviar" />
                    </div>
                </div>
                <div class="row margin-top-50">
                    <div class="column column-40">
                    <!-- Break -->
                    <div class="4u 12u$(small)">
                        <input type="radio" id="demo-priority-low" name="demo-priority" checked>
                        <label for="demo-priority-low">Lunes</label>
                    </div>
                    <div class="4u 12u$(small)">
                        <input type="radio" id="demo-priority-normal" name="demo-priority">
                        <label for="demo-priority-normal">Miercoles</label>
                    </div>
                    <div class="4u$ 12u$(small)">
                        <input type="radio" id="demo-priority-high" name="demo-priority">
                        <label for="demo-priority-high">Viernes</label>
                    </div>
                    </div>
                </div>
                <div class="row margin-top-50">
                    <div class="column column-40">
                    <!-- Break -->
                    <div class="6u 12u$(small)">
                        <input type="checkbox" id="demo-copy" name="demo-copy">
                        <label for="demo-copy">He leido y acepto</label>
                    </div>
                    <div class="6u$ 12u$(small)">
                        <input type="checkbox" id="demo-human" name="demo-human" checked>
                        <label for="demo-human">Suscribirme al newsletter</label>
                    </div>
                    </div>
                </div>
                <div class="row margin-top-50">
                    <div class="column column-40">
                    <!-- Break -->
                    <div class="12u$">
                        <textarea name="demo-message" id="demo-message" placeholder="Enter your message" rows="6"></textarea>
                    </div>
                    </div>
                </div>
                <div class="row margin-top-50">
                    <div class="column column-40">
                    <!-- Break -->
                        <input type="submit" value="Enviar mensaje" /><span>&nbsp;&nbsp;</span>
                        <input type="reset" value="Reset" class="special" />
                    </div>
                </div>
            </form>
            
        </div>
    </div>
    <div class="row margin-top">
        <div class="column">

            <!-- Image -->
            <h2>Imágenes</h2>

            <h3>Imagen destacada: ancho 100%</h3>
            <img class="fit-img" src="assets/images/blog-header.jpg" alt="" />

            <h4>Imagen de contenido: izquierda, derecha y centro</h4>
            <p><img class="alignleft" src="assets/images/featured-3.jpg" alt="" />Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget.</p>
            
            <p><img class="alignright" src="assets/images/featured-6.jpg" alt="" />Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus.</p>
            
            <p>Lorem ipsum dolor sit accumsan interdum nisi, quis tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus. Integer ac pellentesque praesent tincidunt felis sagittis eget. tempus euismod. Vestibulum ante ipsum primis sagittis eget. tempus euismod. Vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing accumsan eu faucibus.</p>
            <img class="aligncenter" src="assets/images/featured-3.jpg" alt="" />
        
        </div>
    </div>
    
    <hr class="alt" />
</section>

<?php include("featured-products.php"); ?>

<?php include("footer.php"); ?>