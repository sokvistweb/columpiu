/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

$(document).ready(function() {
    
    
    /* hide-shop Products panel */
    var clickedOnBody = true;
          
    $('#advanced_search_btn,.advanced_search').click(function(){
        clickedOnBody  = false;
    });


    $('.advanced_search').hide();

    $('#advanced_search_btn,.advanced_search').hover(function() {
        clearTimeout(timeout);
        $('.advanced_search').fadeIn(300);
    });

    var timeout;

    function hidepanel() {
        $('.advanced_search').fadeOut(150); 
    }

    $('.advanced_search').mouseleave(doTimeout);
    $('#advanced_search_btn').mouseleave(doTimeout);

    function doTimeout(){
        clearTimeout(timeout);
        timeout = setTimeout(hidepanel, 300);
    }

    $("html").click(function(){
        if (clickedOnBody){
            $('.advanced_search').hide();
        }
        clickedOnBody=true;
    });
    
    
    
    /* Cart form show/hide */
    var hoverTimeout, keepOpen = false, stayOpen = $('.widget_shopping_cart');
    
    $(document).on('mouseenter','.bag, .cart-contents',function(){
        clearTimeout(hoverTimeout);
        stayOpen.fadeIn(300);
    }).on('mouseleave','.bag, .cart-contents',function(){
        clearTimeout(hoverTimeout);
        hoverTimeout = setTimeout(function(){
            if(!keepOpen){
                stayOpen.fadeOut(150);
            }
        },300);
    });

    $(document).on('mouseenter','.widget_shopping_cart',function(){
        keepOpen = true;
        setTimeout(function(){
            keepOpen = false;
        },1500);
    }).on('mouseleave','.widget_shopping_cart',function(){
        keepOpen = false;
        stayOpen.fadeOut(150); 
    });
    
    
    
    // http://responsiveslides.com v1.54 by @viljamis
    // Slideshow 1
    $("#slider-main").responsiveSlides({
        auto: true,
        speed: 900,
        timeout: 4000,
        pager: true,
        nav: false,
        namespace: "centered-btns"
    });
    
    
    
    // https://owlcarousel2.github.io/OwlCarousel2/
    // Owl Carousel 2
    $('#owl-carousel,#owl-carousel2,#owl-carousel3').owlCarousel({
        loop: false,
        margin: 10,
        dots: true,
        nav: false,
        responsive: {
            0: {
                items:1
            },
            600: {
                items:2
            },
            1000: {
                items:4
            }
        }
    })
    
    $('#owl-carousel-product').owlCarousel({
        loop: false,
        margin: 0,
        dots: true,
        nav: false,
        autoWidth: false,
        items: 1
    })
    
    
    
    // Sticky-kit v1.1.3 | MIT | Leaf Corcoran 2015 | http://leafo.net
    // https://github.com/leafo/sticky-kit
    //$("#sidebar").stick_in_parent();
    
    
    
    // Tabs https://inspirationalpixels.com/creating-tabs-with-html-css-and-jquery/
    /*$('.tabs .tab-links a').on('click', function(e) {
		var currentAttrValue = jQuery(this).attr('href');

		// Show/Hide Tabs
		jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

		// Change/remove current tab to active
		jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

		e.preventDefault();
	});*/
    
    
    
    // Back to top
    // browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 900,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 2000,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 1000,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		 	}, scroll_top_duration
		);
	});
    
    
});

