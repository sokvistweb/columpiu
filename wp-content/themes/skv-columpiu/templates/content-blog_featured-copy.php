<?php 
/* Template Name: Blog Featured
   Displays featured content of the blog */
?>
   

<section class="wrapper margin-top" id="blog-section">
    <hr>
    <div class="row row-center">
       <div class="column column-20">
            <h2 class="row-title"><a href="/universo-columpiu">Universo Columpiu</a></h2>
        </div>

        <div class="column">
            <div class="grid-container">
                <div class="grid-box a">
                    <?php
                    // Get the ID of a given category
                    $category_id = get_cat_ID( 'Escaparates de Columpiu' );
                    // Get the URL of this category
                    $category_link = get_category_link( $category_id );
                    // Get ACF image
                    $image = get_field('imagen_destacada', 'category_'. $category_id);
                    ?>
                    <a href="<?php echo esc_url( $category_link ); ?>" title="Escaparates de Columpiu">
                        <h3>Escaparates de Columpiu</h3>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                </div>
                <div class="grid-box b">
                    <?php
                    // Get the ID of a given category
                    $category_id = get_cat_ID( 'Columpiu in the world' );
                    // Get the URL of this category
                    $category_link = get_category_link( $category_id );
                    // Get ACF image
                    $image = get_field('imagen_destacada', 'category_'. $category_id);
                    ?>
                    <a href="<?php echo esc_url( $category_link ); ?>" title="Columpiu in the world">
                        <h3>Columpiu in the world</h3>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                </div>
                <div class="grid-box c">
                    <?php
                    // Get the ID of a given category
                    $category_id = get_cat_ID( 'Historia de Columpiu' );
                    // Get the URL of this category
                    $category_link = get_category_link( $category_id );
                    // Get ACF image
                    $image = get_field('imagen_destacada', 'category_'. $category_id);
                    ?>
                    <a href="<?php echo esc_url( $category_link ); ?>" title="Historia de Columpiu">
                        <h3>Historia de Columpiu</h3>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>