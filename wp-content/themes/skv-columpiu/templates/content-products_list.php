<?php 
/* Template Name: Products list
   Displays products categories on header menu */
?>
   
    
<section class="search">
    <div class="advanced_search">
        <div class="wrapper">
            <!-- Menu Categories -->
            <div class="cats-content">
                <nav class="cats-nav">
                    <ul class="ul-cats">
                        <?php wp_list_categories( array(
                            'taxonomy' => 'product_cat',
                            'exclude'  => array( 15, 132 ),
                            'title_li' => '<h3>' . __( 'Categorias', 'textdomain' ) . '</h3>',
                            'hide_empty' => '1'
                        ) ); ?> 
                    </ul>
                    
                    <ul class="ul-styles">
                        <?php wp_list_categories( array(
                            'taxonomy' => 'estilos',
                            'title_li' => '<h3>' . __( 'Estilos', 'textdomain' ) . '</h3>',
                            'hierarchical' => true,
                            'hide_empty' => '1'
                        ) ); ?>
                    </ul>
                    
                </nav>
            </div>
            <!-- /Menu Categories -->
        </div>
    </div><!--  end advanced search section  -->
</section><!--  end search section  -->