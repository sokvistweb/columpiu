<?php 
    /* Template Name: Featured
       Displays featured products */
?>
   

<section class="wrapper margin-top-60 featured" id="featured">
    <hr class="alt-full" />
    <h2 class="row-title">Productos destacados</h2>
    <div class="row">
        <div class="column">
            <div class="owl-carousel owl-theme owl-skv-theme" id="owl-carousel">
                <?php
                // https://createandcode.com/fix-broken-featured-products-woocommerce-3-0/
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 12,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_visibility',
                            'field'    => 'name',
                            'terms'    => 'featured',
                        ),
                    ),
                );
                $featured_query = new WP_Query( $args );  
                if ($featured_query->have_posts()) :   
                while ($featured_query->have_posts()) :   
                $featured_query->the_post();  
                $product = get_product( $featured_query->post->ID );
                ?>
                
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                <?php the_post_thumbnail('medium'); ?>
                            <?php endif; ?>
                        </a>
                    </div>
                    <?php do_action('woocommerce_before_shop_loop_item'); ?>
                    <a href="<?php the_permalink(); ?>" class="buy-btn">Comprar</a>
                    <h3 class="product-name woocommerce-loop-product__title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                </div>
                
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>