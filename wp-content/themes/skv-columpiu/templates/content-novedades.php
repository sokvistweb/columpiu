<?php 
    /* Template Name: Novedades
       Displays featured products */
?>
   

<section class="wrapper margin-top-60 featured">
    <hr class="alt-full" />
    <h2 class="row-title">Novedades</h2>
    <div class="row">
        <div class="column">
            <div class="owl-carousel owl-theme owl-skv-theme" id="owl-carousel">
                <?php if (have_posts()) : ?>
                <?php query_posts(array( 'post_type' => 'product', 'order' => 'DESC', 'posts_per_page' => 12 )); ?>
                <?php while (have_posts()) : the_post(); ?>
                
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                <?php the_post_thumbnail('medium'); ?>
                            <?php endif; ?>
                        </a>
                    </div>
                    <?php do_action('woocommerce_before_shop_loop_item'); ?>
                    <a href="<?php the_permalink(); ?>" class="buy-btn">Comprar</a>
                    <h3 class="product-name woocommerce-loop-product__title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                </div>
                
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>