<?php 
/* Template Name: Blog Featured
   Displays featured content of the blog */
?>
   

<section class="wrapper margin-top" id="blog-section">
    <hr class="alt-full" />
    <h2 class="row-title"><a href="/columpiu/universo-columpiu">Blog de Columpiu</a></h2>
    <div class="row">
        <div class="column">
            <div class="grid-box">
                <?php
                // Get the ID of a given category
                $category_id = get_cat_ID( 'Escaparates de Columpiu' );
                // Get the URL of this category
                $category_link = get_category_link( $category_id );
                // Get ACF image
                $image = get_field('imagen_destacada', 'category_'. $category_id);
                ?>
                <a href="<?php echo esc_url( $category_link ); ?>" title="Escaparates de Columpiu">
                    <h3>Escaparates de Columpiu</h3>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </div>
        </div>

        <div class="column">
            <div class="grid-box">
                <?php
                // Get the ID of a given category
                $category_id = get_cat_ID( 'Columpiu in the world' );
                // Get the URL of this category
                $category_link = get_category_link( $category_id );
                // Get ACF image
                $image = get_field('imagen_destacada', 'category_'. $category_id);
                ?>
                <a href="<?php echo esc_url( $category_link ); ?>" title="Columpiu in the world">
                    <h3>Columpiu in the world</h3>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </div>
        </div>

        <div class="column">
            <div class="grid-box">
                <?php
                // Get the ID of a given category
                $category_id = get_cat_ID( 'Historia de Columpiu' );
                // Get the URL of this category
                $category_link = get_category_link( $category_id );
                // Get ACF image
                $image = get_field('imagen_destacada', 'category_'. $category_id);
                ?>
                <a href="<?php echo esc_url( $category_link ); ?>" title="Historia de Columpiu">
                    <h3>Antiguos Columpius</h3>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </div>
        </div>

        <div class="column">
            <div class="grid-box">
                <?php
                // Get the ID of a given category
                $category_id = get_cat_ID( 'Viejas glorias' );
                // Get the URL of this category
                $category_link = get_category_link( $category_id );
                // Get ACF image
                $image = get_field('imagen_destacada', 'category_'. $category_id);
                ?>
                <a href="<?php echo esc_url( $category_link ); ?>" title="Historia de Columpiu">
                    <h3>Viejas glorias</h3>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
            </div>
        </div>
    </div>
</section>