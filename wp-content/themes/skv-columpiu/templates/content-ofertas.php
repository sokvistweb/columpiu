<?php 
    /* Template Name: Ofertas
       Displays featured products */
?>
   

<section class="wrapper margin-top-60 featured">
    <hr class="alt-full" />
    <h2 class="row-title">Ofertas</h2>
    <div class="row">
        <div class="column">
            <div class="owl-carousel owl-theme owl-skv-theme" id="owl-carousel">
                <?php
                // https://www.skyverge.com/blog/get-a-list-of-woocommerce-sale-products/
                // https://stackoverflow.com/questions/20990199/woocommerce-display-only-on-sale-products-in-shop
                // https://createandcode.com/fix-broken-featured-products-woocommerce-3-0/
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 12,
                    'meta_query'     => array(
                        'relation' => 'OR',
                        array( // Simple products type
                            'key'           => '_sale_price',
                            'value'         => 0,
                            'compare'       => '>',
                            'type'          => 'numeric'
                        )
                    ),
                );
                $loop = new WP_Query( $args );  
                if ($loop->have_posts()) :   
                while ($loop->have_posts()) :   
                $loop->the_post();  
                $product = get_product( $loop->post->ID );
                ?>
                
                <div class="featured-product">
                    <div class="scale-effect">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                                <?php the_post_thumbnail('medium'); ?>
                            <?php endif; ?>
                        </a>
                    </div>
                    <?php do_action('woocommerce_before_shop_loop_item'); ?>
                    <a href="<?php the_permalink(); ?>" class="buy-btn">Comprar</a>
                    <h3 class="product-name woocommerce-loop-product__title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                </div>
                
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>