<?php /* Template Name: Ofertas */ get_header(); ?>


<section class="wrapper margin-top-40 page">
    <div class="row">
        <div class="column">

			<h1><?php the_title(); ?></h1>
			<hr />
        
        </div>
    </div>
    
    <div class="row">
        <div class="column">

		    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<?php the_content(); ?>

		    <?php endwhile; ?>
		    <?php else: ?>

            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		    <?php endif; ?>

		</div>
    </div>

</section>

<?php get_template_part( 'templates/content', 'novedades' ); ?>

<?php get_template_part( 'templates/content', 'featured' ); ?>


<?php get_footer(); ?>
