<?php /* Template Name: Página Atrezzo */ get_header(); ?>


<section class="wrapper margin-top-40 page">
    <div class="row">
        <div class="column">

			<h1><?php the_title(); ?></h1>
			<hr />
        
        </div>
    </div>
    
    
    <div class="row">
        <div class="column">

		    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<?php the_content(); ?>

		    <?php endwhile; ?>
		    <?php else: ?>

            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		    <?php endif; ?>

		</div>
    </div>
</section>


<section class="wrapper margin-top-50 page">
    <div class="row">
        <div class="column">
            <ul class="shop-grid products atrezzo-grid">
		    <?php if (have_posts()) : ?>
            <?php query_posts(array( 'post_type' => 'atrezo', 'posts_per_page' => 60 )); ?>
            <?php while (have_posts()) : the_post(); ?>
            <li class="product-atrezzo">
                <div class="rslides slider-atrezzo">

                    <?php 
                    $image = get_field('imagen_1');
                    $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                    }
                    ?>

                    <?php 
                    $image = get_field('imagen_2');
                    $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                    }
                    ?>

                    <?php 
                    $image = get_field('imagen_3');
                    $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                    }
                    ?>

                    <?php 
                    $image = get_field('imagen_4');
                    $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                    }
                    ?>

                </div>
				<h2 class="product-name woocommerce-loop-product__title"><?php the_title(); ?></h2>
            </li>
		    <?php endwhile; ?>
		    <?php else: ?>

            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		    <?php endif; ?>
            </ul>
		</div>
    </div>

</section>


<?php get_footer(); ?>
