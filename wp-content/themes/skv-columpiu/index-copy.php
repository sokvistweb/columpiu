<?php get_header(); ?>

<section class="wrapper margin-top-20 page">
    <div class="row">
        <div class="column">
            <h1>Universo Columpiu</h1>
            <hr />
        </div>
    </div>
    
    <div class="row">
        <div class="column">
            <ul class="blog-grid posts">
                <?php if (have_posts()) : ?>
                <?php query_posts(array( 'post_type' => 'post', 'paged' => $paged )); ?>
                <?php while (have_posts()) : the_post(); ?>
                <li class="product-card">
                    <div class="scale-effect">
                        <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_post_thumbnail('medium'); ?></a>
                        <?php endif; ?>
                    </div>
                    <h2 class="post-title"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <p><?php the_excerpt(); ?></p>
                    <div>
                        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="read">Leer</a>
                    </div>
                </li>
                <?php endwhile; ?>
                <?php endif; ?>
            </ul>
		</div>
    </div>
    
    <div class="row margin-top-50">
        <div class="column">
            <?php wp_numeric_posts_nav(); ?>
        </div>
    </div>
    
    <hr class="alt" />

</section>

<?php get_template_part( 'templates/content', 'featured' ); ?>

<?php get_footer(); ?>
