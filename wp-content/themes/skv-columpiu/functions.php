<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

//if (!isset($content_width))
//{
//    $content_width = 900;
//}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');
    
    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 1140, '', true); // Large Thumbnail
    add_image_size('medium_large', 0, 0, true); // Medium large Thumbnail
    add_image_size('medium', 600, 600, true); // Medium Thumbnail
    add_image_size('small', 0, 0, true); // Small Thumbnail
    add_image_size('thumbnail', 0, 0, true); // Small Thumbnail

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav() {
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="main-menu">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}


// Footer navigation
function footer_nav() {
    wp_nav_menu(
    array(
        'theme_location'  => 'footer-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="legal-pages">%3$s</ul>',
        'depth'           => 0,
        'walker'          => ''
        )
    );
}


// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'footer-menu' => __('Footer Menu', 'html5blank')//, Footer Navigation
        //'product-menu' => __('Product Menu', 'html5blank') // Product Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
/*function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}*/

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
//add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Atrezzo
function create_post_type_html5() {
    register_taxonomy_for_object_type('category', 'atrezo'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'atrezo');
    register_post_type('atrezo', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Atrezzo', 'html5blank'), // Rename these to suit
            'singular_name' => __('Atrezzo Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Atrezzo Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Atrezzo Custom Post', 'html5blank'),
            'new_item' => __('New Atrezzo Custom Post', 'html5blank'),
            'view' => __('View Atrezzo Custom Post', 'html5blank'),
            'view_item' => __('View Atrezzo Custom Post', 'html5blank'),
            'search_items' => __('Search Atrezzo Custom Post', 'html5blank'),
            'not_found' => __('No Atrezzo Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No Atrezzo Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            //'editor',
            //'excerpt',
            //'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            //'post_tag',
            //'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}



/*------------------------------------*\
	SKV-Columpiu Theme Support
\*------------------------------------*/
// Remove Default jQuery from WordPress Head (Frontend)
/*function remove_default_scripts( ){
      wp_deregister_script( 'jquery' );
}
add_filter( 'wp_enqueue_scripts', 'remove_default_scripts');*/


// remove emoji icons (WP 4.2)
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


// Getting rid of wp-embed.min.js
// https://wordpress.stackexchange.com/questions/211701/what-does-wp-embed-min-js-do-in-wordpress-4-4
function my_deregister_scripts() {
    wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/*
 * Remove the `wp-block-library.css` file from `wp_head()`
 * https://wpcrux.com/blog/remove-gutenberg-enqueued-css/
 * @author Rahul Arora
 * @since  12182018
 * @uses   wp_dequeue_style
 */
add_action( 'wp_enqueue_scripts', function() {
    wp_dequeue_style( 'wp-block-library' );
} );



/** 
 * Remove admin menu items from users in WordPress
 * https://www.jeanphilippemarchand.com/code/remove-specific-admin-menu-items-users-wordpress/
 */
add_action('admin_menu', 'remove_admin_menu_links');
function remove_admin_menu_links(){
    $user = wp_get_current_user();
    if( $user && isset($user->user_email) && 'joandolot@gmail.com' == $user->user_email ) {
        remove_menu_page('tools.php');
        remove_menu_page('themes.php');
        remove_menu_page('options-general.php');
        remove_menu_page('plugins.php');
        remove_menu_page('edit.php?post_type=acf-field-group'); 
        //remove_menu_page('users.php');
        //remove_menu_page('edit-comments.php');
        //remove_menu_page('page.php');
        //remove_menu_page('upload.php');
        //remove_menu_page('edit.php?post_type=page'); 
        //remove_menu_page('edit.php?post_type=videos');
        //remove_menu_page('edit.php');
		
        // Plugins
		remove_menu_page('smush');
    }
}


// Remove Contact Form 7 javascript
/*add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 ); // Change 100 to the id of the contact form page
function deregister_cf7_javascript() {
    if ( !is_page(100) ) {
        wp_deregister_script( 'contact-form-7' );
    }
}*/

// Remove Contact Form 7 stylesheet
add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 ); // Change 100 to the id of the contact form page
function deregister_cf7_styles() {
    if ( !is_page(100) ) {
        wp_deregister_style( 'contact-form-7' );
    }
}



// Custom login page
// https://codex.wordpress.org/Customizing_the_Login_Form
function my_login_logo() { ?>
    <style type="text/css">
        body.login {
            background: #ffffff;
        }
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/columpiu-logo.svg) !important;
            background-size: 180px 60px !important;
            width: 180px !important;
            height: 60px !important;
            margin: 0 auto 10px !important;
            padding-bottom: 0 !important;
        }
        .login a {
            color: #1D171A;
        }
        .login a:hover {
            color: #DA291C;
        }
        .login form {
            margin-top: 0 !important;
            border: 1px solid #1D171A;
            background-color: #fff !important;
            border-radius: 0 !important;
            box-shadow: none !important;
        }
        body.login div#login form#loginform p.submit input#wp-submit {
            font-weight: bold;
            text-transform: uppercase;
            color: #1D171A;
            background: white;
            border-color: #1D171A;
            border-top: 1px solid white;
            border-left: 1px solid white;
            border-right: 1px solid white;
            border-bottom: 1px solid #1D171A;
            border-radius: 0;
            text-shadow: none;
            box-shadow: none;
        }
        body.login div#login form#loginform p.submit input#wp-submit:hover,
        body.login div#login form#loginform p.submit input#wp-submit:focus {
            background: #fff;
            border-bottom-color: #DA291C;
            color: #DA291C;
        }
        body.login div#login p#nav a:hover,
        body.login div#login p#backtoblog a:hover {
            color: #DA291C;
        }
        body.login #login_error,
        body.login .message {
            background-color: #fff;
            border-top: 1px solid rgba(29, 23, 26, 0.25);
            border-right: 1px solid rgba(29, 23, 26, 0.25);
            border-bottom: 1px solid rgba(29, 23, 26, 0.25);
            border-radius: 0 !important;
            box-shadow: none !important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Columpiu | Els mobles de la iaia';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


// Add excerpt to page
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() 
{
     add_post_type_support( 'page', 'excerpt' );
}


/**
 * Add custom taxonomies
 * https://www.smashingmagazine.com/2012/01/create-custom-taxonomies-wordpress/
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function custom_taxonomy() {
    register_taxonomy('estilos', 'product', array(
        'hierarchical' => true,
        'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
        
    'labels' => array(
        'name'                       => _x( 'Estilos', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Estilo', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Estilos', 'text_domain' ),
		'all_items'                  => __( 'Todos los estilos', 'text_domain' ),
		'parent_item'                => __( 'Estilo padre', 'text_domain' ),
		'parent_item_colon'          => __( 'Estilo padre:', 'text_domain' ),
		'new_item_name'              => __( 'Nuevo nombre de Estilo', 'text_domain' ),
		'add_new_item'               => __( 'Añadir nuevo Estilo', 'text_domain' ),
		'edit_item'                  => __( 'Editar Estilo', 'text_domain' ),
		'update_item'                => __( 'Actualizar Estilo', 'text_domain' ),
		'view_item'                  => __( 'Ver Estilo', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separar Estilos con comas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Añadir o eliminiar Estilos', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'estilos', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'custom_taxonomy', 0 );



// Blog Pagination
// http://www.wpbeginner.com/wp-themes/how-to-add-numeric-pagination-in-your-wordpress-theme/
function wp_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul class="pagination">' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link('&laquo;') );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s" class="page">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s" class="page">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s" class="page">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link('&raquo;') );

	echo '</ul>' . "\n";

}


add_action('pre_user_query','yoursite_pre_user_query');
function yoursite_pre_user_query($user_search) {
  global $current_user;
  $username = $current_user->user_login;

  if ($username != 'sergi') { 
    global $wpdb;
    $user_search->query_where = str_replace('WHERE 1=1',
      "WHERE 1=1 AND {$wpdb->users}.user_login != 'skv-admin'",$user_search->query_where);
  }
}

/*----------------------------------------*\
   Sokvist-Columpiu WooCommerce functions
\*----------------------------------------*/

// https://docs.woocommerce.com/document/disable-the-default-stylesheet/
// Disable the default stylesheet
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


/**
 * Manage WooCommerce styles and scripts.
 */
function grd_woocommerce_script_cleaner() {
	
	// Remove the generator tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
	// Unless we're in the store, remove all the cruft!
	if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() && ! is_page( array( 'novedades', 'ofertas' ) ) ) {
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'wc-cart-fragments' );
		wp_dequeue_script( 'wc-checkout' );
		wp_dequeue_script( 'wc-add-to-cart-variation' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-cart' );
		wp_dequeue_script( 'wc-chosen' );
		wp_dequeue_script( 'woocommerce' );
		wp_dequeue_script( 'jquery-blockui' );
		wp_dequeue_script( 'jqueryui' );
	}
}
add_action( 'wp_enqueue_scripts', 'grd_woocommerce_script_cleaner', 99 );



// Add support for Woocommerce for custom theme
// https://wordpress.org/support/topic/single-product-is-using-single-php-not-single-product-php/
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


// Thumbnail image sizes https://github.com/woocommerce/woocommerce/wiki/Customizing-image-sizes-in-3.3
/*add_theme_support( 'woocommerce', array(
    'thumbnail_image_width'         => 600,
    'gallery_thumbnail_image_width' => 600,
    'single_image_width'            => 600,
) );*/

add_filter( 'woocommerce_get_image_size_thumbnail', function( $size ) {
    return array(
        'width'  => 600,
        'height' => 600,
        'crop'   => 1,
    );
} );

add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
    return array(
        'width'  => 600,
        'height' => 600,
        'crop'   => 1,
    );
} );

add_filter( 'woocommerce_get_image_size_single', function( $size ) {
    return array(
        'width'  => 600,
        'height' => 600,
        'crop'   => 1,
    );
} );




/* 
 * Custom WooCommerce Shop Loop Product Thumbnail and Title
 * https://gist.github.com/ben-heath/5f7031a4ff393e5db9b41ec0df3446cc
 */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    // remove product thumbnail and title from the shop loop
    remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
    remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
    
    // add the product thumbnail and title back in with custom structure
    add_action( 'woocommerce_before_shop_loop_item_title', 'sls_woocommerce_template_loop_product_thumbnail', 10 );
    function sls_woocommerce_template_loop_product_thumbnail() {
       echo '<div class="scale-effect"><a class="thumbnail" title="'.get_the_title().'" href="'. get_the_permalink() . '">'.woocommerce_get_product_thumbnail().'</a></div>';
       echo '<h2 class="product-name woocommerce-loop-product__title"><a href="'.get_the_permalink().'">'.get_the_title().'</a></h2>';
    } 
    
}


/* Remove link before thumbnail
 * https://businessbloomer.com/woocommerce-visual-hook-guide-archiveshopcat-page/ */
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 ); 


/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 32;
  return $cols;
}



/**
 * Change several of the breadcrumb defaults
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &gt; ',
            'wrap_before' => '<ul id="breadcrumbs" class="breadcrumbs">',
            'wrap_after'  => '</ul>',
            'before'      => '<li>',
            'after'       => '</li>',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}


/*
 * Reposition WooCommerce breadcrumb 
 * https://wordpress.stackexchange.com/questions/99925/reposition-woocommerce-breadcrumb-outside-of-wrapper-content
 */
function woocommerce_remove_breadcrumb() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
}
add_action( 'woocommerce_before_main_content', 'woocommerce_remove_breadcrumb' );

function woocommerce_custom_breadcrumb() {
    woocommerce_breadcrumb();
}
add_action( 'woo_custom_breadcrumb', 'woocommerce_custom_breadcrumb' );



/*
 * Remove image links from WooCommerce thumbnail images
 */
add_filter('woocommerce_single_product_image_thumbnail_html','wc_remove_link_on_thumbnails' );
 
function wc_remove_link_on_thumbnails( $html ) {
     return strip_tags( $html,'<div><img>' );
}


/**
 * Hide WooCommerce Product Quantity Field from Product Pages
 * https://www.cloudways.com/blog/hide-product-quantity-field-from-woocommerce-product-pages/
 */
function cw_remove_quantity_fields( $return, $product ) {
    return true;
}
add_filter( 'woocommerce_is_sold_individually', 'cw_remove_quantity_fields', 10, 2 );


/**
 * Remove single product tabs and add the related content instead In Woocommerce
 * https://stackoverflow.com/questions/49196441/remove-single-product-tabs-and-add-the-related-content-instead-in-woocommerce
 */
add_action( 'woocommerce_after_single_product_summary', 'removing_product_tabs', 2 );
function removing_product_tabs(){
    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
    add_action( 'woocommerce_after_single_product_summary', 'get_product_tab_templates_displayed', 10 );
}
function get_product_tab_templates_displayed() {
    wc_get_template( 'single-product/tabs/description.php' );
    wc_get_template( 'single-product/tabs/additional-information.php' );
    comments_template();
}


/**
 * Add a DIV around a description and additional description
 * https://stackoverflow.com/questions/35712353/add-a-div-around-a-specific-hook-in-woocommerce
 */
add_action('woocommerce_after_single_product_summary', 'hooks_open_div', 7);
function hooks_open_div() {
    echo '<div class="details wc-product-details">';
}

add_action('woocommerce_after_single_product_summary', 'hooks_close_div', 33);
function hooks_close_div() {
    echo '</div>';
}


/**
 * Add a DIV around a description and additional description
 * https://stackoverflow.com/questions/35712353/add-a-div-around-a-specific-hook-in-woocommerce
 */
add_action('woocommerce_before_shop_loop', 'hooks_open_div2', 7);
function hooks_open_div2() {
    echo '<div class="before-grid">';
}

add_action('woocommerce_before_shop_loop', 'hooks_close_div2', 33);
function hooks_close_div2() {
    echo '</div>';
}



/**
 * Remove related products
 * https://stackoverflow.com/questions/52219588/move-related-product-to-the-sidebar-on-woocommerce-single-products
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products',20);




/**
 * Cart count updating
 * https://stackoverflow.com/questions/52021533/custom-cart-count-is-not-updating-without-reloading-in-woocommerce
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'refresh_cart_count', 50, 1 );
function refresh_cart_count( $fragments ){
    ob_start();
    ?>
    <span class="counter" id="cart-count">
        <?php
        $cart_count = WC()->cart->get_cart_contents_count();
        echo sprintf ( _n( '%d', '%d', $cart_count ), $cart_count );
        ?>
    </span>
    <?php
    
    $fragments['#cart-count'] = ob_get_clean();

    return $fragments;
}



/**
 * Woocommerce add to cart ajax and mini-cart
 * https://stackoverflow.com/questions/39969280/woocommerce-add-to-cart-ajax-and-mini-cart
 */
add_filter( 'woocommerce_add_to_cart_fragments', function($fragments) {
    ob_start();
    ?>
    <div class="cart-dropdown-inner">
        <?php woocommerce_mini_cart(); ?>
    </div>

    <?php $fragments['div.cart-dropdown-inner'] = ob_get_clean();

    return $fragments;

} );



/**
 * Remove the result count
 * https://hirejordansmith.com/how-to-remove-the-woocommerce-results-count/
 */
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );



/**
 * Change the add to cart text on single product pages
 * https://remicorson.com/woocommerce-check-if-product-is-already-in-cart/
 */
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text');
function woo_custom_cart_button_text() {
	
	foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];
	
		if( get_the_ID() == $_product->id ) {
			return __('Ya está en carrito', 'woocommerce');
		}
	}
	
	return __('Add to cart', 'woocommerce');
}

// Change the add to cart text on product archives
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );
function woo_archive_custom_cart_button_text() {
	
	foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];
	
		if( get_the_ID() == $_product->id ) {
			return __('Ya está en carrito', 'woocommerce');
		}
	}
	
	return __('Add to cart', 'woocommerce');
}



/*
 * Make Woocommerce Products Sold Individually By Default
 * https://www.philowen.co/blog/make-woocommerce-products-sold-individually-by-default/
 */
function default_no_quantities( $individually, $product ){
$individually = true;
return $individually;
}
add_filter( 'woocommerce_is_sold_individually', 'default_no_quantities', 10, 2 );



/**
 * Get Woocommerce to Manage Stock as a default
 * https://stackoverflow.com/questions/15908411/get-woocommerce-to-manage-stock-as-a-default
 * ************** (Sergi: not working) **************
 */
/*function wc_default_variation_stock_quantity() {
  global $pagenow, $woocommerce;

  $default_stock_quantity = 1;
  $screen = get_current_screen();

  if ( ( $pagenow == 'post-new.php' || $pagenow == 'post.php' || $pagenow == 'edit.php' ) && $screen->post_type == 'product' ) {

    ?>
<!-- uncomment this if jquery if it hasn't been included-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript">
    jQuery(document).ready(function(){
        if ( !jQuery( '#_manage_stock' ).attr('checked') ) {
          jQuery( '#_manage_stock' ).attr('checked', 'checked');
        }
        if ( '' === jQuery( '#_stock' ).val() ) {
          jQuery( '#_stock' ).val(<?php echo $default_stock_quantity; ?>);
        }
    });
    </script>
    <?php
  }
}
add_action( 'admin_enqueue_scripts', 'wc_default_variation_stock_quantity' );*/



/**
 * Get Woocommerce to Manage Stock as a default
 * https://wordpress.stackexchange.com/questions/291992/automatically-check-the-option-enable-stock-management-at-product-level-on-pro
 */
$postType = "product";

add_action("save_post_" . $postType, function ($post_ID, \WP_Post $post, $update) {

    if (!$update) {
        // default values for new products
        update_post_meta($post->ID, "_manage_stock", "yes");
        update_post_meta($post->ID, "_stock", 1);
        return;
    }
    // here, operations for updated products
}, 10, 3);




/**
* @snippet   Remove Default Sorting Option @ WooCommerce Shop
* @how-to    Watch tutorial @ https://businessbloomer.com/?p=19055
* @sourcecode    https://businessbloomer.com/?p=75511
* @author    Rodolfo Melogli
* @testedwith    WooCommerce 3.5.2
* @donate $9     https://businessbloomer.com/bloomer-armada/
*/
 
add_filter( 'woocommerce_catalog_orderby', 'bbloomer_remove_sorting_option_woocommerce_shop' );
 
function bbloomer_remove_sorting_option_woocommerce_shop( $options ) {
    unset( $options['rating'] );
    unset( $options['popularity'] );
    return $options;
}
 
// Note: you can unset another default sorting option... here's the list:
// 'menu_order' => __( 'Default sorting', 'woocommerce' ),
// 'popularity' => __( 'Sort by popularity', 'woocommerce' ),
// 'rating'     => __( 'Sort by average rating', 'woocommerce' ),
// 'date'       => __( 'Sort by newness', 'woocommerce' ),
// 'price'      => __( 'Sort by price: low to high', 'woocommerce' ),
// 'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),

/**
* @snippet   Rename a Default Sorting Option @ WooCommerce Shop
* @how-to    Watch tutorial @ https://businessbloomer.com/?p=19055
* @sourcecode    https://businessbloomer.com/?p=75511
* @author    Rodolfo Melogli
* @testedwith    WooCommerce 3.5.2
* @donate $9     https://businessbloomer.com/bloomer-armada/
*/
 
add_filter( 'woocommerce_catalog_orderby', 'bbloomer_rename_sorting_option_woocommerce_shop' );
 
function bbloomer_rename_sorting_option_woocommerce_shop( $options ) {
    $options['date'] = __( 'Ordenar por novedad', 'woocommerce' ); 
    return $options;
}



/**
 *
 * Remove CSS and/or JS for Select2 used by WooCommerce
 * https://gist.github.com/Willem-Siebe/c6d798ccba249d5bf080.
 
  ***** Custom select not working on Mozilla (Sergi) *****
 
 */
/*add_action( 'wp_enqueue_scripts', 'wsis_dequeue_stylesandscripts_select2', 100 );

function wsis_dequeue_stylesandscripts_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'select2' );
        wp_deregister_style( 'select2' );

        wp_dequeue_script( 'select2');
        wp_deregister_script('select2');

    } 
}*/


/**
 * Remove sale badge (onlase) on single product page
 * https://generatepress.com/forums/topic/remove-sale-badge-single-product-page/
 */
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );



/**
 * @snippet       Display "Sold Out" on Loop Pages - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=17420
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.4.3
 */
 
add_action( 'woocommerce_before_shop_loop_item_title', 'bbloomer_display_sold_out_loop_woocommerce' );
 
function bbloomer_display_sold_out_loop_woocommerce() {
    global $product;
 
    if ( !$product->is_in_stock() ) {
        echo '<span class="soldout">' . __( 'VENDIDO', 'woocommerce' ) . '</span>';
    }
}


/**
 * @snippet       Hide SKU @ Single Product Page - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=21715
 * @author        Rodolfo Melogli
 * @compatible    WC 3.4.4
 */
 
add_filter( 'wc_product_sku_enabled', 'bbloomer_remove_product_page_sku' );
 
function bbloomer_remove_product_page_sku( $enabled ) {
    if ( !is_admin() && is_product() ) {
        return false;
    }
 
    return $enabled;
}


/**
 * Hide text editor from product pages
 * https://wordpress.stackexchange.com/questions/72865/is-it-possible-to-remove-wysiwyg-for-a-certain-custom-post-type
 */
add_action('init', 'init_remove_support',100);
function init_remove_support(){
    $post_type = 'product';
    remove_post_type_support( $post_type, 'editor');
}



/**
 * Hide the "In stock" message on product page.
 * https://gist.github.com/claudiosanches/c250f1c660ccb81fc76b0968d50b514b
 * @param string $html
 * @param string $text
 * @param WC_Product $product
 * @return string
 */
function my_wc_hide_in_stock_message( $html, $text, $product ) {
	$availability = $product->get_availability();
	if ( isset( $availability['class'] ) && 'in-stock' === $availability['class'] ) {
		return '';
	}
	return $html;
}
add_filter( 'woocommerce_stock_html', 'my_wc_hide_in_stock_message', 10, 3 );




/**
 * Remove the product description Title
 * https://www.codegearthemes.com/blogs/woocommerce/woocommerce-how-to-remove-description-text-in-product-description
 */
add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
 return '';
}



/*add_filter( 'get_the_terms', 'custom_product_cat_terms', 20, 3 );
function custom_product_cat_terms( $terms, $post_id, $taxonomy ){
    // HERE below define your excluded product categories Term IDs in this array
    $category_ids = array( 15, 127, 132 );

    if( ! is_product() ) // Only single product pages
        return $terms;

    if( $taxonomy != 'product_cat' ) // Only product categories custom taxonomy
        return $terms;

    foreach( $terms as $key => $term ){
        if( in_array( $term->term_id, $category_ids ) ){
            unset($terms[$key]); // If term is found we remove it
        }
    }
    return $terms;
}*/


/** Remove categories from shop and other pages
 * in Woocommerce
 */
/*function get_subcategory_terms( $terms, $taxonomies, $args ) {

  $new_terms = array();
  // if a product category and on the shop page
  if ( in_array( 'product_cat', $taxonomies ) && ! is_product() ) {
    foreach ( $terms as $key => $term ) {
      if ( ! in_array( $term->slug, array( 'lotes' ) ) ) {
        $new_terms[] = $term;
      }
    }
    $terms = $new_terms;
  }
  return $terms;
}

add_filter( 'get_terms', 'get_subcategory_terms', 10, 3 );*/




/**
 * Removing the Product Meta ‘Categories’ in a Product Page
 * https://wpbeaches.com/removing-the-product-meta-categories-in-a-product-page-woocommerce/
 */
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );




?>
