<?php get_header(); ?>

<section class="wrapper margin-top-40 page">
    <div class="row">
        <div class="column">

			<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
			<hr />
        
        </div>
    </div>
    
    <div class="row">
        <div class="column">
            <ul>
            <li><a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a></li>
            <li><a href="/tienda/" title="Tienda">¿Ir a la tienda?</a></li>
            </ul>
		</div>
    </div>
    
    <hr class="alt" />

</section>

<?php get_template_part( 'templates/content', 'ofertas' ); ?>

<?php get_template_part( 'templates/content', 'featured' ); ?>

<?php get_footer(); ?>
