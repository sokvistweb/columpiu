<?php get_header(); ?>


<section class="wrapper margin-top-40" id="arrivals">
    <div class="row">
        <div class="column">
            <h2 class="row-title"><a href="/novedades/" title="Novedades">Novedades</a></h2>
             <!-- Slider -->
            <ul class="rslides" id="slider-main">
                <?php if (have_posts()) : ?>
                <?php query_posts(array( 'post_type' => 'product', 'order' => 'DESC', 'posts_per_page' => 10 )); ?>
                <?php while (have_posts()) : the_post(); ?>
                <li><div class="outline-effect scale-effect"><a href="/novedades/" title="Novedades">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('medium'); ?>
                    <?php endif; ?>
                </a></div></li>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </ul>
        </div>

        <div class="column column-3"></div>

        <div class="column">
            <?php query_posts('post_type=page&name=tienda'); while (have_posts ()): the_post(); ?>
            <h1 class="row-title"><a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>"><?php the_excerpt(); ?></a></h1>
            <div class="our-story-image scale-effect">
                <a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('medium'); ?>
                    <?php endif; ?>
                </a>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</section>


<section class="wrapper margin-top" id="main-ctas">
    <div class="row">
        <div class="column w-tagline">
            <?php query_posts('post_type=page&name=vaciados'); while (have_posts ()): the_post(); ?>
            <h2 class="row-title"><a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>"><?php the_title(); ?></a></h2>
            <p><?php the_excerpt(); ?></p>
            <div class="scale-effect">
                <a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('medium'); ?>
                    <?php endif; ?>
                </a>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>

        <div class="column w-tagline">
            <?php query_posts('post_type=page&name=atrezzo'); while (have_posts ()): the_post(); ?>
            <h2 class="row-title"><a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>"><?php the_title(); ?></a></h2>
            <p><?php the_excerpt(); ?></p>
            <div class="scale-effect">
                <a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('medium'); ?>
                    <?php endif; ?>
                </a>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>

        <div class="column w-tagline">
            <?php query_posts('post_type=page&name=ofertas'); while (have_posts ()): the_post(); ?>
            <h2 class="row-title"><a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>"><?php the_title(); ?></a></h2>
            <div class="scale-effect">
                <a href="<?php the_permalink(); ?>" title="<?php the_excerpt(); ?>">
                    <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                        <?php the_post_thumbnail('medium'); ?>
                    <?php endif; ?>
                </a>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</section>


<section class="wrapper margin-top" id="sub-headers">
    <div class="row row-center">
        <div class="column">
            <div>
                <?php query_posts('post_type=page&name=transporte-y-devoluciones'); while (have_posts ()): the_post(); ?>
                <h3 class="row-title"><a href="<?php the_permalink(); ?>" class="a-dotted" title="<?php the_title(); ?>"><?php the_excerpt(); ?></a></h3>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
        
        <div class="column">
            <div>
                <h3 class="row-title">Todos los artículos son originales de época</h3>
            </div>
        </div>
    </div>
</section>


<?php get_template_part( 'templates/content', 'featured' ); ?>

<?php get_template_part( 'templates/content', 'blog_featured' ); ?>


<?php get_footer(); ?>
