<?php /* Template Name: Novedades */ get_header(); ?>


<section class="wrapper margin-top-40 page">
    <div class="row">
        <div class="column">

			<h1><?php the_title(); ?> hola?</h1>
			<hr />

        </div>
    </div>
    
    <div class="row">
        <div class="column">
        
		    <?php if (have_posts()) : ?>
           
            <?php query_posts(array( 'post_type' => 'product', 'posts_per_page' => 50, 'paged' => $paged )); ?>
            
            <ul class="shop-grid products">
            <?php while (have_posts()) : the_post(); ?>
            
				<?php woocommerce_get_template_part('content', 'product'); ?>
            
		    <?php endwhile; ?>
		    </ul>
		    <?php else: ?>

            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

		    <?php endif; ?>
		    
		    
		    <div class="row margin-top-50">
                <nav class="column">
                    <?php wp_numeric_posts_nav(); ?>
                </nav>
            </div>

		</div>
    </div>
    
    <hr class="alt" />

</section>

<?php get_template_part( 'templates/content', 'featured' ); ?>


<?php get_footer(); ?>
