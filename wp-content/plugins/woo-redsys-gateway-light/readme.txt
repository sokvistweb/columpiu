=== WooCommerce Redsys Gateway Light ===
Contributors: j.conti
Tags: woocommerce, redsys, pasarela redsys, gateway, redsys gateway, redsys pasarela, redsys woocommerce, woocommerce redsys, iupay, Iupay gateway, Iupay woocommerce, woocommerce iupay, iupay pasarela, pasarela iupay
Requires at least: 4.0
Tested up to: 4.9.8
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=U7XNYWPL8VRHE
Stable tag: 1.2.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
WC requires at least: 3.0
WC tested up to: 3.4

Add Redsys Gateway (and Iupay) to WooCommerce. This is the Light version of the official WooCommerce Redsys plugin at WooCommerce.com

== Description ==

= Light version Features =

This is the Light version of the official WooCommerce Redsys plugin at WooCommerce.com

You can find the PRO version at [WooCommerce.com](https://woocommerce.com/products/redsys-gateway/)

With this extension you get all you need for use Redsys & Iupay Gateway.

* Always compatible with latest WooCommerce version & continuous audits by WooCommerce Team.
* WPML compatible.
* Works with SNI certificates like Let's Encrypt, EX: SiteGround, HostFusion, etc
* Gateway language selection from settings.
* Checkout logo customization.
* Added checkout logo customization.

As this is WooCommerce official extension, always will be compatible with the latest WooCommerce version.

Why is not it compatible with versions of WooCommerce lower than 2.9? Because they have vulnerabilities and I will not support versions that you should not use.

= Premium version Features =

* Always compatible with Continuous audits by WooCommerce Team.
* WPML compatible.
* Works with SNI certificates like Let's Encrypt, EX: SiteGround, HostFusion, etc
* Gateway language selection from settings.
* Checkout logo customization.
* Iupay Gateway is included with its own setting page
* Tokenization
* Pay with one click
* Preauthorizations
* Approve preauthorizations from WooCommerce order
* Direct Debit
* Private Products
* Second Terminal number. Very useful for security purpose.
* Sequential Invoice Number, essential in Spain by the Public Treasury.
* Refund from Order.
* Error action selection, what do you want that happen when a user make an error on the Gateway?
* Export Order to CSV, export all date orders between two dates to CSV.
* Pay with 1 click.
* And more to come.

== Installation ==

 * Unzip the files and upload the folder into your plugins folder (wp-content/plugins/) overwriting old versions if they exist
 * Activate the plugin in your WordPress admin area.
 * Open the settings page for WooCommerce and click the "Payment Gateways" tab
 * Click on the sub tab for "Redsys/Servired"
 * Configure your Redsys settings.

 And if you want

 * Click on the sub tab for "Iupay"
 * Configure your Iupay settings.

== Frequently Asked Questions ==

== Screenshots ==

1. Welcome screen: Latest updates & premium version.
2. Redsys: Redsys settings screenshot.
3. Iupay: Iupay settings screenshot.
4. Language: Set the Redsys Gateway Language.

== Changelog ==


== 1.2.1 ==
* Fixed a problem in some server settings that the plugin crash at activation.

== 1.2.0 ==
* Removed iupay and added payment options in Redsys setting page. Now you can select if you want iupay or not form settings.
* Fix: Fixed a bug with amounts less than 1€.

== 1.1.1 ==
* Removed message about Mcrypt when PHP is 7.0 or above

== 1.1.0 ==
* Added Redsys API for PHP 5.x and 7.x
* Added ability for customize checkout logo.

= 1.0.1 =
* NEW: Added logo customization
* Updated spinner. This update improve gateway redirection.

= 1.0.0 =
* First public release.


== Upgrade Notice ==
